# Templaring

Temlparing stands for "Template Rendering". This script can replace string from a template file like configuration files or repetitive command lines with different values.


### Requirements

This script has been tested on Linux/Windows with python3.10 or higher.
The following packages are required:
 - numpy
 - pandas
 - xlrd
 - termcolor

It's recommended to crate a virtual environment, activate it and then install the packages:

For Windows:

```sh
> git clone https://github.com/Pr1meSuspec7/templaring.git
> cd templaring
> python -m venv VENV-NAME
> VENV-NAME\Scripts\activate.bat
> pip install -r requirements.txt
```
>NOTE: chose a name for virtual environment and replace the `VENV-NAME` string

For Linux:

```sh
$ git clone https://github.com/Pr1meSuspec7/templaring.git
$ cd templaring
$ python -m venv VENV-NAME
$ source VENV-NAME/bin/activate
$ pip install -r requirements.txt
```
>NOTE: chose a name for virtual environment and replace the `VENV-NAME` string


### How it works

You need to create two files:
 - ***Excel file*** that contains parameters that must be change in the template
 - ***Text file*** to use as a template

The column titles in the excel file (***don't use spaces***) are the patterns that will be replaced in the template.\
The template will be looped for each row in the excel file.\
If you want to create a file for each row you will have to use a different filename.\
If you use the same filename on multiple rows the result will be appended to that file.\
Files will be stored in a new folder called "rendering" that is create in to working path.\

Example excel file:

FILENAME     | HOSTNAME | GIGABIT00   | GIGABIT00_MASK | GIGABIT01 | GIGABIT01_MASK
------------ | -------- | ----------- | -------------- | --------- | --------------
ROUTER1.cfg  | R1       | 192.168.1.1 | 255.255.255.0  | 10.0.1.1  | 255.255.255.0
ROUTER2.cfg  | R2       | 192.168.2.1 | 255.255.255.0  | 10.0.2.1  | 255.255.255.0
ROUTER3.cfg  | R3       | 192.168.3.1 | 255.255.255.0  | 10.0.3.1  | 255.255.255.0

Example template file:

	hostname HOSTNAME 
		
	interface GigabitEthernet0/0
	 no shut
	 ip address GIGABIT00 GIGABIT00_MASK
		
	interface GigabitEthernet0/1 
	 no shut
	 ip address GIGABIT01 GIGABIT01_MASK


### How to use

```sh
$ python main.py
```

The prompt will ask you the name of excel file, the name of the sheet in the excel and template file.\
You can rename your files with the default values (**data.xlsx** for Excel file, **Sheet1** for sheet's Excel, **template.templ** for template file), or you can digit the name of your files present in to working folder.

```sh
If you want stop this script type: exit

Insert the name of the excel file [default: data.xlsx]:
Insert the name of the sheet in the excel file [default: Sheet1]:
Insert the name of the template file [default: template.txt]:
```

After run the script you'll find rendered files in to "rendering" folder.

```sh
Building rendering...
ROUTER1.cfg
ROUTER2.cfg
ROUTER3.cfg

!!! RENDERING COMPLETED !!!

Check the folder --> /home/user/templaring/rendering
```

Check rendered files, you can see the variables replaced with the values from the table:

```sh
$ cat /home/user/templaring/rendering/ROUTER1.cfg

hostname R1 
	
interface GigabitEthernet0/0
 no shut
 ip address 192.168.1.1 255.255.255.0
	
interface GigabitEthernet0/1 
 no shut
 ip address 10.0.1.1 255.255.255.0
'''
