import os
from re import sub
from termcolor import colored
import pandas as pd
import platform

# Activate color output for Windows cmd
if platform.system() == 'Windows':
    os.system('color')

# Functions to check if files and sheet exist
def file_exist(files):
    if not os.path.isfile(files):
        print(colored("\nERROR ---> No such file or directory: " + files, 'red'))
        quit()

def sheet_exist(sheetname):
    xl = pd.ExcelFile(excelfile)
    try:
        xl.parse(sheetname)
    except:
        print(colored('\nThis sheet not exist: ' + sheetname, 'red'))
        quit()

# Function to replace template variables with excel sheet values 
def replace_content(dictionary, file):
    for dictionarykey, dictionaryvalue in list(dictionary.items()):
        file = sub(dictionarykey, str(dictionaryvalue), file)
    return file

# Function to create the target file using the HOSTNAME value of the excel sheet
def destfile(filename, filemodify):
    newfile = open(filename, 'a')
    newfile.write(filemodify)
    newfile.close()
    print(filename)

# Function to write the conf file inside the new folder
def rendering():
    os.chdir(conf_folder)
    for index, row in xls.iterrows():
        params = row.tolist()
        name = str(params[0])
        if name == 'nan':
           print(colored('\nFILENAME column cannot be empty!', 'red'))
           quit()
        else:
           dic = dict(zip(dictkeys, params))
           result = replace_content(dic, txt)
           destfile(name, result)

#######################################

# Initial banner
print("""
*********************************************************************************
*********************************************************************************
***                                                                           ***
***   This script use an excel file with a template to generate rendering     ***
***                                                                           ***
*********************************************************************************
*********************************************************************************
"""
)
print(colored('If you want stop this script type:'), colored('exit', 'yellow'))
print('\n')

# Select excel file
excelfile = str(input('Insert the name of the excel file [default: data.xlsx]:'))
if excelfile == 'exit':
    print(colored('Cancel by the user.', 'yellow'))
    quit()
elif not excelfile:
    excelfile = 'data.xlsx'

# Select excel sheet
excelsheet = str(input('Insert the name of the sheet in the excel file [default: Sheet1]:'))
if excelsheet == 'exit':
    print(colored('Cancel by the user.', 'yellow'))
    quit()
elif not excelsheet:
    excelsheet = 'Sheet1'

# Select template file
templatebase = str(input('Insert the name of the template file [default: template.templ]:'))
if templatebase == 'exit':
    print(colored('Cancel by the user.', 'yellow'))
    quit()
elif not templatebase:
    templatebase = 'template.templ'

# Check if excel and template exist
files = [excelfile, templatebase]
for i in files:
    file_exist(i)

# Check if sheet exist
#xl = pd.ExcelFile(excelfile)
sheet_exist(excelsheet)

# If not exist create new folder called "rendering" into working path
cwd = os.getcwd()
conf_folder = cwd + '/rendering'

if not os.path.exists(conf_folder):
    os.mkdir(conf_folder)
else:
    print(colored('\nFolder "rendering" already exist.\n', 'yellow'))

# Load the excel file, template file and extracts dictonary keys from columns of excel file
os.chdir(cwd)
xls = pd.read_excel(excelfile, sheet_name=excelsheet)
txt = open(templatebase, 'r+').read()
dictkeys = list(xls.columns)

# Main function
def main():
    print('\nBuilding rendering...')
    rendering()

    print(colored('\n!!! RENDERING COMPLETED !!!\n', 'green'))
    print(colored('Check the folder --> ' + conf_folder, 'yellow'))

# Top-level code
if __name__ == "__main__":
    main()
